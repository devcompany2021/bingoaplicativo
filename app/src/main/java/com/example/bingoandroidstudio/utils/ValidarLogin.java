package com.example.bingoandroidstudio.utils;

import android.os.AsyncTask;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ValidarLogin extends AsyncTask<Void, Void, User> {

    private final String user;
    private final String pass;

    public ValidarLogin(String usuario, String senha) {
        this.user = usuario;
        this.pass = senha;
    }

    @Override
    protected User doInBackground(Void... voids) {
        String resposta = "";
            try {

                URL url = new URL("http://192.168.0.119:5000/bingo/api/auth/signin");

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json; utf-8");
                connection.setRequestProperty("Accept", "application/json");
                connection.setDoOutput(true);
                connection.setConnectTimeout(5000);
                String jsonInputString = "{\"username\": \""+user+"\", \"password\": \""+pass+"\"}";
                try(OutputStream os = connection.getOutputStream()) {
                    byte[] input = jsonInputString.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(connection.getInputStream(), "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                    resposta=response.toString();
                }
                connection.connect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        return new Gson().fromJson(resposta, User.class);
    }
}
