package com.example.bingoandroidstudio.utils;

public class Print {
    private int qtde_jogos;
    private double valor_jogo;
    private Object linha;
    private int tipo;
    private String texto;
    private String alinhamento;

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getAlinhamento() {
        return alinhamento;
    }

    public void setAlinhamento(String alinhamento) {
        this.alinhamento = alinhamento;
    }

    public Object getLinha() {

        return linha;
    }

    public void setLinha(Object linha) {
        this.linha = linha;
    }

    public int getQtde_jogos() {
        return qtde_jogos;
    }

    public void setQtde_jogos(int qtde_jogos) {
        this.qtde_jogos = qtde_jogos;
    }

    public double getValor_jogo() {
        return valor_jogo;
    }

    public void setValor_jogo(double valor_jogo) {
        this.valor_jogo = valor_jogo;
    }

    @Override
    public String toString() {
        return "linha: "+getLinha();
    }
}
