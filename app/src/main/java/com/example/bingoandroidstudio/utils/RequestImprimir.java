package com.example.bingoandroidstudio.utils;

import android.os.AsyncTask;

import com.example.bingoandroidstudio.activity.ConectarActivity;
import com.example.bingoandroidstudio.activity.LoginActivity;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class RequestImprimir extends AsyncTask<Void, Void, String> {

    private int qtde;
    private double valor;

    public RequestImprimir(int qtde, double valor) {
        this.qtde = qtde;
        this.valor = valor;
    }

    @Override
    protected String doInBackground(Void... voids) {
        String resposta = "";
            try {
                URL url = new URL("http://192.168.0.119:5000/bingo/NovoJogo");

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json; utf-8");
                connection.setRequestProperty("Accept", "application/json");
                String token = "Bearer "+ LoginActivity.Token;
                connection.setRequestProperty("Authorization",token);
                connection.setDoOutput(true);
                connection.setConnectTimeout(5000);
                String jsonInputString = "{\"qtde_jogos\": \""+qtde+"\", \"valor_jogo\": \""+valor+"\"}";
                try(OutputStream os = connection.getOutputStream()) {
                    byte[] input = jsonInputString.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(connection.getInputStream(), "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                        System.out.println(responseLine.trim());
                    }
                    resposta=response.toString();
                }
                connection.connect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        return resposta;
    }
}
