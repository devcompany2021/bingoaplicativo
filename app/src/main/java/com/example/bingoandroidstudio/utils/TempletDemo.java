package com.example.bingoandroidstudio.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Message;
import android.util.JsonWriter;
import android.util.Log;

import com.example.bingoandroidstudio.R;
import com.example.bingoandroidstudio.app.BaseApplication;
import com.rt.printerlibrary.bean.LableSizeBean;
import com.rt.printerlibrary.bean.Position;
import com.rt.printerlibrary.cmd.Cmd;
import com.rt.printerlibrary.cmd.CpclFactory;
import com.rt.printerlibrary.cmd.EscFactory;
import com.rt.printerlibrary.cmd.TscFactory;
import com.rt.printerlibrary.cmd.ZplFactory;
import com.rt.printerlibrary.enumerate.BarcodeStringPosition;
import com.rt.printerlibrary.enumerate.BarcodeType;
import com.rt.printerlibrary.enumerate.BmpPrintMode;
import com.rt.printerlibrary.enumerate.CommonEnum;
import com.rt.printerlibrary.enumerate.CpclFontTypeEnum;
import com.rt.printerlibrary.enumerate.ESCFontTypeEnum;
import com.rt.printerlibrary.enumerate.EscBarcodePrintOritention;
import com.rt.printerlibrary.enumerate.PrintDirection;
import com.rt.printerlibrary.enumerate.PrintRotation;
import com.rt.printerlibrary.enumerate.QrcodeEccLevel;
import com.rt.printerlibrary.enumerate.SettingEnum;
import com.rt.printerlibrary.enumerate.TscFontTypeEnum;
import com.rt.printerlibrary.enumerate.ZplFontTypeEnum;
import com.rt.printerlibrary.exception.SdkException;
import com.rt.printerlibrary.factory.cmd.CmdFactory;
import com.rt.printerlibrary.printer.RTPrinter;
import com.rt.printerlibrary.setting.BarcodeSetting;
import com.rt.printerlibrary.setting.BitmapSetting;
import com.rt.printerlibrary.setting.CommonSetting;
import com.rt.printerlibrary.setting.TextSetting;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TempletDemo {
    private RTPrinter rtPrinter;
    private Context context;
    String title, content_tel, content_email, barcode_str;
    private static  TempletDemo templetDemo;

    public TempletDemo(RTPrinter rtPrinter, Context context){
        this.rtPrinter = rtPrinter;
        this.context = context;
    }
    public  static TempletDemo getInstance(RTPrinter rtPrinter, Context context){
        if (templetDemo==null)
            templetDemo = new TempletDemo(rtPrinter,context);
         else{
            templetDemo.rtPrinter = rtPrinter;
            templetDemo.context = context;
        }
        templetDemo.initdata();
        return  templetDemo;
    }
    private void initdata(){
        title = context.getString(R.string.temp1_title1_printer_tech);
        content_tel = context.getString(R.string.temp1_content1_tel);
        content_email = context.getString(R.string.temp1_content2_email);
        barcode_str = "123456789";
    }

    public void escTemplet() throws UnsupportedEncodingException, SdkException, JSONException {
        /*TODO aqui vai receber um json com os dados de impressão, linha a linha

        o json tera o seguinte modelo
        {
            1:{
            "tipo":1,
            "alinhamento":"left",
            "texto":"Mega Bingo",
            },
            2:{
            "tipo":1,
            "texto":"Local.:Bar",
            },
            3:{
            "tipo":1,
            "texto":"DATA..:31/05/2021 13:00",
            },
            4:{
            "tipo":1,
            "texto":"Sorteio.:3452",
            },
            5:{
            "tipo":1,
            "texto":"DOACAO.: 2,00",
            }
        }

         */
        JSONArray json = new JSONArray();
        JSONObject linha1 = new JSONObject();
        linha1.put("tipo",1);
        linha1.put("texto","Mega Bingo\nLocal...: Bar\nDATA....: 31/05/2021 13:00\nSORTEIO.: 3452\nDOACAO..: 2,00\nCODIGO..: 194 3833703\n--------------------------------\nDOADO: 2,00 - 1 CUPOM\nDE: 165 ATE 165\n--------------------------------");
        linha1.put("alinhamento","left");
        json.put(1,linha1);

        JSONObject linha2 = new JSONObject();
        linha2.put("tipo",2);
        linha2.put("texto","No 165 SORTEIO: 3452\n");
        linha2.put("alinhamento","middle");
        json.put(2,linha2);

        JSONObject linha3 = new JSONObject();
        linha3.put("tipo",1);
        linha3.put("texto","--------------------------------\n");
        linha3.put("alinhamento","middle");
        json.put(3,linha3);

        JSONObject linha4 = new JSONObject();
        linha4.put("tipo",3);
        linha4.put("texto","02|20|48|71|87\n");
        linha4.put("alinhamento","middle");
        json.put(4,linha4);

        JSONObject linha5 = new JSONObject();
        linha5.put("tipo",1);
        linha5.put("texto","--------------------------------\n");
        linha5.put("alinhamento","middle");
        json.put(5,linha5);

        JSONObject linha6 = new JSONObject();
        linha6.put("tipo",3);
        linha6.put("texto","04|35|44|56|81\n");
        linha6.put("alinhamento","middle");
        json.put(6,linha6);

        JSONObject linha7 = new JSONObject();
        linha7.put("tipo",1);
        linha7.put("texto","--------------------------------\n");
        linha7.put("alinhamento","middle");
        json.put(7,linha7);

        JSONObject linha8 = new JSONObject();
        linha8.put("tipo",3);
        linha8.put("texto","06|33|49|69|90\n");
        linha8.put("alinhamento","middle");
        json.put(8,linha8);

        JSONObject linha9 = new JSONObject();
        linha9.put("tipo",1);
        linha9.put("texto","--------------------------------\n");
        linha9.put("alinhamento","middle");
        json.put(9,linha9);

        JSONObject linha10 = new JSONObject();
        linha10.put("tipo",2);
        linha10.put("texto","\nDOADO: 2,00 - 1 CUPOM");
        linha10.put("alinhamento","left");
        json.put(10,linha10);
        /*
        JSONObject linha5 = new JSONObject();
        linha5.put("tipo",1);
        linha5.put("texto","");
        linha5.put("alinhamento","left");
        json.put(5,linha5);
        JSONObject linha6 = new JSONObject();
        linha6.put("tipo",1);
        linha6.put("texto","");
        linha6.put("alinhamento","left");
        json.put(6,linha6);
        JSONObject linha7 = new JSONObject();
        linha7.put("tipo",2);
        linha7.put("texto","");
        linha7.put("alinhamento","middle");
        json.put(7,linha7);
        JSONObject linha8 = new JSONObject();
        linha8.put("tipo",2);
        linha8.put("texto","");
        linha8.put("alinhamento","middle");
        json.put(8,linha8);
        JSONObject linha9 = new JSONObject();
        linha9.put("tipo",3);
        linha9.put("texto","");
        linha9.put("alinhamento","middle");
        json.put(9,linha9);


         */
        CmdFactory escFac = new EscFactory();
        Cmd escCmd = escFac.create();
        escCmd.setChartsetName("UTF-8");
        TextSetting textSetting = new TextSetting();
/*
        HashMap<Integer,Object>json=new HashMap<>();
        HashMap<String,Object>linha=new HashMap<>();
        linha.put("tipo",1);
        linha.put("texto","Mega Bingo");
        json.put(1,linha);
        linha.clear();
        linha.put("tipo",1);
        linha.put("texto","Local.:Bar");
        json.put(2,linha);
        linha.clear();
        linha.put("tipo",1);
        linha.put("texto","DATA..:31/05/2021 13:00");
        json.put(3,linha);
        linha.clear();
        linha.put("tipo",1);
        linha.put("texto","Sorteio.:3452");
        json.put(4,linha);
        linha.clear();
        linha.put("tipo",1);
        linha.put("texto","DOACAO.: 2,00");
        json.put(5,linha);
        linha.clear();


        textSetting.setAlign(CommonEnum.ALIGN_MIDDLE);//Alinhamento - esquerda, centro, direita
        textSetting.setBold(SettingEnum.Disable);//Negrito
        textSetting.setUnderline(SettingEnum.Disable);//Sublinhado
        textSetting.setIsAntiWhite(SettingEnum.Disable);//Anti-branco
        textSetting.setDoubleHeight(SettingEnum.Disable);//Altura dupla
        textSetting.setDoubleWidth(SettingEnum.Disable);//Largura dupla
        textSetting.setItalic(SettingEnum.Disable);//itálico
        textSetting.setIsEscSmallCharactor(SettingEnum.Disable);//Fonte pequena
/*
        //TODO aqui vai um for lendo o json que vira do back

        
 */

        escCmd.append(escCmd.getHeaderCmd()); //inicia a impressão
        for (int i = 1; i < json.length(); i++) {
            JSONObject linhai = new JSONObject();
            linhai=json.optJSONObject(i);
            try {
                switch (linhai.opt("tipo").toString()){
                    case "1":
                        textSetting.setBold(SettingEnum.Enable);//Negrito
                        textSetting.setDoubleHeight(SettingEnum.Disable);//Altura dupla
                        textSetting.setDoubleWidth(SettingEnum.Disable);//Largura dupla
                        break;
                    case "2":
                        textSetting.setBold(SettingEnum.Enable);//Negrito
                        textSetting.setDoubleHeight(SettingEnum.Enable);//Altura dupla
                        textSetting.setDoubleWidth(SettingEnum.Disable);//Largura dupla
                        break;
                    case "3":
                        textSetting.setBold(SettingEnum.Enable);//Negrito
                        textSetting.setDoubleHeight(SettingEnum.Enable);//Altura dupla
                        textSetting.setDoubleWidth(SettingEnum.Enable);//Largura dupla
                        break;
                    default:
                        break;
                }
                switch (linhai.opt("alinhamento").toString()){
                    case "left":
                        textSetting.setAlign(CommonEnum.ALIGN_LEFT);//Alinhamento - esquerda, meio, direita
                        break;
                    case "middle":
                        textSetting.setAlign(CommonEnum.ALIGN_MIDDLE);//Alinhamento - esquerda, meio, direita
                        break;
                    case "right":
                        textSetting.setAlign(CommonEnum.ALIGN_RIGHT);//Alinhamento - esquerda, meio, direita
                        break;
                    default:
                        break;
                }
                escCmd.append(escCmd.getTextCmd(textSetting,linhai.opt("texto").toString() ));
            }catch (Exception e) {
                    e.printStackTrace();
            }
        }
        /*
        for (int i = 0; i <= json.size(); i++) {
            linha= (HashMap<String, Object>) json.get(i);
            for(String j : linha.keySet()){
                System.out.println("i: "+i+".j: "+j+".linha: "+linha.get(j));
            }
        }

        /*for (int i = 0; i <= json.size(); i++) {
            System.out.println("key: " + i + " value: " + json.get(i));
            String[] chave = json.get(i).toString().split("@@");
            switch (chave[1]) {
                case "align":
                    System.out.println("Vai Alinhar tudo a partir daqui a "+String.valueOf(json.get(i)));
                    switch (String.valueOf(json.get(i))){
                        case "left":
                            textSetting.setAlign(CommonEnum.ALIGN_LEFT);
                            break;
                        case "center":
                            textSetting.setAlign(CommonEnum.ALIGN_MIDDLE);
                            break;
                        case "right":
                            textSetting.setAlign(CommonEnum.ALIGN_RIGHT);
                            break;
                        default:
                            break;
                    }
                    break;
                case "bold":
                    if(String.valueOf(json.get(i))=="enable"){
                        textSetting.setBold(SettingEnum.Enable);
                        System.out.println("Vai deixar tudo em negrito a partir daqui");
                    }else if(String.valueOf(json.get(i))=="disable"){
                        textSetting.setBold(SettingEnum.Disable);
                        System.out.println("Vai tirar o negrito a partir daqui");
                    }
                    break;
                case "texto":
                    System.out.println("Vai imprimir "+String.valueOf(json.get(i)));
                    //escCmd.append(escCmd.getTextCmd(textSetting, String.valueOf(json.get(i))));
                    //escCmd.append(escCmd.getLFCRCmd());
                    break;
                default:
                    break;
            }
        }
        /*
        for (int i : json.keySet()) {
            System.out.println("key: " + i + " value: " + json.get(i));
            /*
            String[] chave=i.split("-");
            System.out.println(chave[0]);
            System.out.println(chave[1]);

            System.out.println("key: " + i + " value: " + json.get(i));
        }*/

        /* conjunto de text settings
        textSetting.setAlign(CommonEnum.ALIGN_MIDDLE);//Alinhamento - esquerda, centro, direita
        textSetting.setBold(SettingEnum.Enable);//Negrito
        textSetting.setUnderline(SettingEnum.Disable);//Sublinhado
        textSetting.setIsAntiWhite(SettingEnum.Disable);//Anti-branco
        textSetting.setDoubleHeight(SettingEnum.Enable);//Altura dupla
        textSetting.setDoubleWidth(SettingEnum.Enable);//Largura dupla
        textSetting.setItalic(SettingEnum.Disable);//itálico
        textSetting.setIsEscSmallCharactor(SettingEnum.Disable);//Fonte pequena
         */
        //escCmd.append(escCmd.getHeaderCmd());//inicialização
        //escCmd.append(escCmd.getTextCmd(textSetting, "Mega Bingo"));
        //escCmd.append(escCmd.getLFCRCmd());//Retorno de carruagem
/*
        textSetting.setIsEscSmallCharactor(SettingEnum.Enable);
        textSetting.setBold(SettingEnum.Disable);
        textSetting.setDoubleHeight(SettingEnum.Disable);
        textSetting.setDoubleWidth(SettingEnum.Disable);
        escCmd.append(escCmd.getTextCmd(textSetting, content_tel));

        escCmd.append(escCmd.getLFCRCmd());
        textSetting.setUnderline(SettingEnum.Enable);
        escCmd.append(escCmd.getTextCmd(textSetting, content_email));

        escCmd.append(escCmd.getLFCRCmd());
        escCmd.append(escCmd.getLFCRCmd());

        BitmapSetting bitmapSetting = new BitmapSetting();
        bitmapSetting.setBimtapLimitWidth(40);//限制图片最大宽度 58打印机=48mm， 80打印机=72mm

        Bitmap bmp  = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);

        escCmd.append(escCmd.getBitmapCmd(bitmapSetting, Bitmap.createBitmap(bmp)));
        escCmd.append(escCmd.getLFCRCmd());

        BarcodeSetting barcodeSetting = new BarcodeSetting();
        barcodeSetting.setEscBarcodePrintOritention(EscBarcodePrintOritention.Rotate0);
        barcodeSetting.setBarcodeStringPosition(BarcodeStringPosition.BELOW_BARCODE);
        barcodeSetting.setHeightInDot(72);//accept value:1~255
        barcodeSetting.setBarcodeWidth(3);//accept value:2~6
        escCmd.append(escCmd.getBarcodeCmd(BarcodeType.CODE128, barcodeSetting, barcode_str));
        escCmd.append(escCmd.getLFCRCmd());

        barcodeSetting.setQrcodeDotSize(5);//accept value: Esc(1~15), Tsc(1~10)
        barcodeSetting.setQrcodeEccLevel(QrcodeEccLevel.L);
        escCmd.append(escCmd.getBarcodeCmd(BarcodeType.QR_CODE, barcodeSetting, content_email));

        escCmd.append(escCmd.getLFCRCmd());//final da impressão pulando 3 linhas para facilitar o corte
       escCmd.append(escCmd.getLFCRCmd());
        escCmd.append(escCmd.getLFCRCmd());
*/
        escCmd.append(escCmd.getLFCRCmd());
        escCmd.append(escCmd.getLFCRCmd());
        rtPrinter.writeMsgAsync(escCmd.getAppendCmds());
    }

    public void escTicketTemplet() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    CmdFactory escFac = new EscFactory();
                    Cmd escCmd = escFac.create();
                    escCmd.append(escCmd.getHeaderCmd());//初始化
                    escCmd.setChartsetName("UTF-8");

                    CommonSetting commonSetting = new CommonSetting();
                    commonSetting.setAlign(CommonEnum.ALIGN_MIDDLE);
                    escCmd.append(escCmd.getCommonSettingCmd(commonSetting));

                    BitmapSetting bitmapSetting = new BitmapSetting();
                    bitmapSetting.setBimtapLimitWidth(28 * 8);//限制图片最大宽度 58打印机=48mm， 80打印机=72mm
                    Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.logo_rongta);
                    try {
                        escCmd.append(escCmd.getBitmapCmd(bitmapSetting, Bitmap.createBitmap(bmp)));
                    } catch (SdkException e) {
                        e.printStackTrace();
                    }
                    escCmd.append(escCmd.getLFCRCmd());


                    TextSetting textSetting = new TextSetting();
                    textSetting.setAlign(CommonEnum.ALIGN_MIDDLE);//对齐方式-左对齐，居中，右对齐
                    textSetting.setBold(SettingEnum.Enable);//加粗
                    textSetting.setUnderline(SettingEnum.Disable);//下划线
                    textSetting.setIsAntiWhite(SettingEnum.Disable);//反白
                    textSetting.setDoubleHeight(SettingEnum.Enable);//倍高
                    textSetting.setDoubleWidth(SettingEnum.Enable);//倍宽
                    textSetting.setItalic(SettingEnum.Disable);//斜体
                    textSetting.setIsEscSmallCharactor(SettingEnum.Disable);//小字体

                    escCmd.append(escCmd.getTextCmd(textSetting, "The Red Rose"));
                    escCmd.append(escCmd.getLFCRCmd());//回车换行
                    textSetting.setBold(SettingEnum.Disable);
                    textSetting.setDoubleHeight(SettingEnum.Disable);//倍高
                    textSetting.setDoubleWidth(SettingEnum.Disable);//倍宽
                    textSetting.setIsEscSmallCharactor(SettingEnum.Enable);//小字体
                    escCmd.append(escCmd.getTextCmd(textSetting, "Indian Resturant"));
                    escCmd.append(escCmd.getLFCRCmd());
                    escCmd.append(escCmd.getTextCmd(textSetting, "Noida, Noida"));
                    escCmd.append(escCmd.getLFCRCmd());
                    escCmd.append(escCmd.getTextCmd(textSetting, "Website:http://www.xxx.com"));
                    escCmd.append(escCmd.getLFCRCmd());
                    escCmd.append(escCmd.getTextCmd(textSetting, "GSTIN No.:ROS2345ST"));
                    escCmd.append(escCmd.getLFCRCmd());
                    String line = "—————————————————————————————————————————";
                    textSetting.setBold(SettingEnum.Enable);
                    escCmd.append(escCmd.getTextCmd(textSetting, line));
                    escCmd.append(escCmd.getLFCRCmd());
                    textSetting.setBold(SettingEnum.Disable);
                    String bank = "                          ";
                    escCmd.append(escCmd.getTextCmd(textSetting, "Type:" + bank + "Table:2"));
                    escCmd.append(escCmd.getLFCRCmd());//回车换行
                    escCmd.append(escCmd.getTextCmd(textSetting, "Type:" + bank + "Table:2"));
                    escCmd.append(escCmd.getLFCRCmd());//回车换行
                    escCmd.append(escCmd.getTextCmd(textSetting, "Type:" + bank + "Table:2"));
                    escCmd.append(escCmd.getLFCRCmd());//回车换行
                    textSetting.setBold(SettingEnum.Enable);
                    escCmd.append(escCmd.getTextCmd(textSetting, line));
                    escCmd.append(escCmd.getLFCRCmd());

                    escCmd.append(escCmd.getTextCmd(textSetting, "Item Name      Qty       Rate      Amount\n"));

                    textSetting.setBold(SettingEnum.Enable);
                    escCmd.append(escCmd.getTextCmd(textSetting, line));
                    escCmd.append(escCmd.getLFCRCmd());

                    textSetting.setBold(SettingEnum.Disable);
                    escCmd.append(escCmd.getTextCmd(textSetting, "Item111        2.00      83.33    150.0000\n"));
                    escCmd.append(escCmd.getTextCmd(textSetting, "Item111        2.00      83.33    150.0000\n"));
                    escCmd.append(escCmd.getTextCmd(textSetting, "Item111        2.00      83.33    150.0000\n"));
                    escCmd.append(escCmd.getTextCmd(textSetting, "Item111        2.00      83.33    150.0000\n"));
                    escCmd.append(escCmd.getTextCmd(textSetting, "Item111        2.00      83.33    150.0000\n"));

                    textSetting.setBold(SettingEnum.Enable);
                    escCmd.append(escCmd.getTextCmd(textSetting, line));
                    escCmd.append(escCmd.getLFCRCmd());

                    textSetting.setBold(SettingEnum.Disable);
                    escCmd.append(escCmd.getTextCmd(textSetting, "            Sub Total             750.0000\n"));
                    escCmd.append(escCmd.getTextCmd(textSetting, "            @Oval                        0\n"));

                    textSetting.setBold(SettingEnum.Enable);
                    escCmd.append(escCmd.getTextCmd(textSetting, line));
                    escCmd.append(escCmd.getLFCRCmd());

                    textSetting.setIsEscSmallCharactor(SettingEnum.Disable);//小字体
                    textSetting.setBold(SettingEnum.Enable);//加粗
                    escCmd.append(escCmd.getTextCmd(textSetting, "     Net Amount         2524.98\n"));

                    textSetting.setBold(SettingEnum.Enable);
                    textSetting.setIsEscSmallCharactor(SettingEnum.Enable);//小字体
                    escCmd.append(escCmd.getTextCmd(textSetting, line));
                    escCmd.append(escCmd.getLFCRCmd());

                    textSetting.setBold(SettingEnum.Disable);
                    textSetting.setIsEscSmallCharactor(SettingEnum.Enable);//小字体
                    escCmd.append(escCmd.getTextCmd(textSetting, "KOT(s): KOT_23,KOT_24,KOT_31               \n"));
                    escCmd.append(escCmd.getTextCmd(textSetting, "Guest Signature:              ___________\n"));
                    escCmd.append(escCmd.getTextCmd(textSetting, "Authorised Signatory:         ___________\n"));
                    escCmd.append(escCmd.getTextCmd(textSetting, "Cashier:                                   \n"));

                    textSetting.setItalic(SettingEnum.Enable);
                    textSetting.setAlign(CommonEnum.ALIGN_MIDDLE);
                    textSetting.setIsEscSmallCharactor(SettingEnum.Disable);//小字体
                    escCmd.append(escCmd.getTextCmd(textSetting, "Have a nice day.\nThank you visit again"));

                    escCmd.append(escCmd.getLFCRCmd());
                    escCmd.append(escCmd.getLFCRCmd());
                    escCmd.append(escCmd.getLFCRCmd());
                    escCmd.append(escCmd.getLFCRCmd());
                    escCmd.append(escCmd.getLFCRCmd());

                    rtPrinter.writeMsgAsync(escCmd.getAppendCmds());

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    public void zplPrint() throws UnsupportedEncodingException, SdkException {
        if (rtPrinter == null) {
            return;
        }

        CmdFactory fac = new ZplFactory();
        Cmd cmd = fac.create();

        cmd.append(cmd.getHeaderCmd());

        CommonSetting commonSetting = new CommonSetting();
        commonSetting.setLabelGap(2);
        commonSetting.setPrintDirection(PrintDirection.REVERSE);//打印方向
        commonSetting.setLableSizeBean(new LableSizeBean(80, 80));//label width = 80mm, label height = 80mm
        cmd.append(cmd.getCommonSettingCmd(commonSetting));


        TextSetting textSetting = new TextSetting();
        textSetting.setZplFontTypeEnum(ZplFontTypeEnum.FONT_DOWNLOAD_FONT);
        textSetting.setTxtPrintPosition(new Position(130, 80));
        textSetting.setPrintRotation(PrintRotation.Rotate0);
        textSetting.setZplHeightFactor(64);// >10
        textSetting.setZplWidthFactor(64);//>10
        try {
            cmd.append(cmd.getTextCmd(textSetting, title));
            textSetting.setTxtPrintPosition(new Position(160, 140));
            textSetting.setZplHeightFactor(2);// 1~10
            textSetting.setZplWidthFactor(2);// 1~10
            textSetting.setZplFontTypeEnum(ZplFontTypeEnum.FONT_2);
            cmd.append(cmd.getTextCmd(textSetting, content_tel));
            textSetting.setTxtPrintPosition(new Position(70, 180));
            cmd.append(cmd.getTextCmd(textSetting, content_email));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        BitmapSetting bitmapSetting = new BitmapSetting();
        bitmapSetting.setBimtapLimitWidth(40);//限制图片最大宽度 58打印机=48mm， 80打印机=72mm
        bitmapSetting.setPrintPostion(new Position(250, 220));

        Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);

        try {
            cmd.append(cmd.getBitmapCmd(bitmapSetting, Bitmap.createBitmap(bmp)));
        } catch (SdkException e) {
            e.printStackTrace();
        }
        cmd.append(cmd.getLFCRCmd());

        BarcodeSetting barcodeSetting = new BarcodeSetting();
        barcodeSetting.setBarcodeStringPosition(BarcodeStringPosition.BELOW_BARCODE);
        barcodeSetting.setPosition(new Position(120, 280));
        barcodeSetting.setPrintRotation(PrintRotation.Rotate0);
        barcodeSetting.setHeightInDot(72);//accept value:1~255
        barcodeSetting.setBarcodeWidth(3);//accept value:2~6
        try {
            cmd.append(cmd.getBarcodeCmd(BarcodeType.CODE128, barcodeSetting, barcode_str));
        } catch (SdkException e) {
            e.printStackTrace();
        }
        cmd.append(cmd.getLFCRCmd());

        barcodeSetting.setPosition(new Position(200, 420));
        barcodeSetting.setQrcodeDotSize(5);//accept value: Esc(1~15), Tsc(1~10)
        barcodeSetting.setQrcodeEccLevel(QrcodeEccLevel.L);
        try {
            cmd.append(cmd.getBarcodeCmd(BarcodeType.QR_CODE, barcodeSetting, content_email));
        } catch (SdkException e) {
            e.printStackTrace();
        }

        cmd.append(cmd.getLFCRCmd());
        cmd.append(cmd.getLFCRCmd());
        cmd.append(cmd.getLFCRCmd());

        try {
            cmd.append(cmd.getPrintCopies(1));//ZPL must add this function, print copies settings， ZPL必须要加上这个方法，打印份数
        } catch (SdkException e) {
            e.printStackTrace();
        }
        cmd.append(cmd.getEndCmd());
        String str = new String(cmd.getAppendCmds());
        Log.e("sss", str);
        rtPrinter.writeMsg(cmd.getAppendCmds());

    }



}
