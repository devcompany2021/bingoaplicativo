package com.example.bingoandroidstudio.utils;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class BaseEnum {

    public static final int NONE = -1;
    public static final int CMD_ESC = 1;
    public static final int CON_BLUETOOTH = 1, CON_WIFI = 3, CON_USB = 4, CON_COM = 5;
    public static final int NO_DEVICE = -1, HAS_DEVICE = 1;

    @IntDef({CMD_ESC})
    @Retention(RetentionPolicy.SOURCE)
    public @interface CmdType {
    }

    @IntDef({CON_BLUETOOTH, CON_WIFI, CON_USB, CON_COM, NONE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ConnectType {
    }


    @IntDef({NO_DEVICE, HAS_DEVICE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ChooseDevice {
    }



}
