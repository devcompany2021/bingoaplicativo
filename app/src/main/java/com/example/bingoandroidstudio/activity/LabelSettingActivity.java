package com.example.bingoandroidstudio.activity;
//atividade de gração de label, LIXO, mas possui uma variavel usada na base, procurar para limpar

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.example.bingoandroidstudio.R;
import com.example.bingoandroidstudio.app.BaseActivity;
import com.example.bingoandroidstudio.app.BaseApplication;

public class LabelSettingActivity extends BaseActivity implements View.OnClickListener {

    public static final String SP_KEY_LABEL_OFFSET = "labelOffset";
    public static final String SP_KEY_LABEL_SIZE = "labelSize";
    private static final String SP_KEY_LABEL_SPEED = "labelSpeed";
    private String labelSize;
    private SharedPreferences sp;
    private Context mContext;
    private LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        addListener();
        init();
    }

    @Override
    public void initView() {
        mContext = this;
    }

    @Override
    public void addListener() {
    }

    @Override
    public void init() {
        inflater = LayoutInflater.from(this);
        sp = getSharedPreferences(BaseApplication.SP_NAME_SETTING, Context.MODE_PRIVATE);
        labelSize = sp.getString(SP_KEY_LABEL_SIZE, getResources().getStringArray(R.array.label_setting_size)[0]);
        String[] temp = labelSize.split("\\*");
        BaseApplication.labelWidth = temp[0];
        BaseApplication.labelHeight = temp[1];

        BaseApplication.labelSpeed = sp.getString(SP_KEY_LABEL_SPEED, "2");
        BaseApplication.labelOffset = sp.getString(SP_KEY_LABEL_OFFSET, "0");

    }

    @Override
    public void onClick(View v) {
    }

}
