package com.example.bingoandroidstudio.activity;
//Aqui tem a atividade de Codigo de barras, talvez use para geração de qrcode que o paulo quer, analisar
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.bingoandroidstudio.app.BaseApplication;
import com.example.bingoandroidstudio.R;
import com.example.bingoandroidstudio.adapter.BarcodeAdapter;
import com.example.bingoandroidstudio.utils.BaseEnum;
import com.rt.printerlibrary.enumerate.BarcodeType;

import java.util.Arrays;
import java.util.List;

public class BarcodeActivity extends ListActivity {

    private final String TAG = getClass().getSimpleName();
    private Context mContext;
    private LinearLayout back;
    private List<String> tagList;
    private List<String> itemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode);
        mContext = this;
        initView();
        setAdapter();
        setListener();
    }


    private void initView() {
        back = findViewById(R.id.back);
    }

    public void setAdapter() {
        tagList = Arrays.asList(mContext.getResources().getStringArray(R.array.barcode_tag));
        switch (BaseApplication.getInstance().getCurrentCmdType()) {
            case BaseEnum.CMD_ESC:
                itemList = Arrays.asList(mContext.getResources().getStringArray(R.array.barcode_item_esc));
                break;
            default:
                itemList = Arrays.asList(mContext.getResources().getStringArray(R.array.barcode_item_esc));
                break;
        }
        BarcodeAdapter adapter = new BarcodeAdapter(mContext, itemList, tagList);
        setListAdapter(adapter);
    }

    private void setListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        String barcodeType = l.getAdapter().getItem(position).toString();
        if (getString(R.string.QR_CODE).equals(barcodeType)) {
            barcodeType = BarcodeType.QR_CODE.name();
        }
        Intent intent = new Intent(mContext, BarcodePrintActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(BarcodePrintActivity.BUNDLE_KEY_BARCODE_TYPE, barcodeType);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}
