package com.example.bingoandroidstudio.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bingoandroidstudio.R;
import com.example.bingoandroidstudio.app.BaseActivity;
import com.example.bingoandroidstudio.app.BaseApplication;
import com.example.bingoandroidstudio.utils.Print;
import com.example.bingoandroidstudio.utils.RequestImprimir;
import com.example.bingoandroidstudio.utils.User;
import com.example.bingoandroidstudio.utils.ValidarLogin;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.rt.printerlibrary.cmd.Cmd;
import com.rt.printerlibrary.cmd.EscFactory;
import com.rt.printerlibrary.enumerate.CommonEnum;
import com.rt.printerlibrary.enumerate.ESCFontTypeEnum;
import com.rt.printerlibrary.enumerate.SettingEnum;
import com.rt.printerlibrary.factory.cmd.CmdFactory;
import com.rt.printerlibrary.printer.RTPrinter;
import com.rt.printerlibrary.setting.TextSetting;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

public class ImprimirActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, RadioGroup.OnCheckedChangeListener {
    private static final String TAG = ImprimirActivity.class.getSimpleName();
    private Button btn_txtprint, btn_menos, btn_mais;
    private RadioGroup rg_valor;
    private TextView tv_nJogos;
    private RadioButton rb_valor1,rb_valor2,rb_valor3,rb_valor4;
    private int nJogos=1;
    private int maxJogos=4;
    private String vJogo="";

    private RTPrinter rtPrinter;
    private String printStr;
    private TextSetting textSetting;
    private String mChartsetName = "UTF-8";
    private ESCFontTypeEnum curESCFontType = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_print_esc);
        initView();
        addListener();
        init();
    }

    @SuppressLint("WrongViewCast")
    @Override
    public void initView() {
        btn_txtprint = findViewById(R.id.btn_txtprint);
        btn_menos=findViewById(R.id.btn_menos);
        btn_mais=findViewById(R.id.btn_mais);
        rg_valor=findViewById(R.id.rg_valor);
        tv_nJogos=findViewById(R.id.tv_nJogos);
        rb_valor1=findViewById(R.id.rb_valor1);
        rb_valor2=findViewById(R.id.rb_valor2);
        rb_valor3=findViewById(R.id.rb_valor3);
        rb_valor4=findViewById(R.id.rb_valor4);
        vJogo=rb_valor1.getText().toString();
    }

    @Override
    public void addListener() {
        btn_txtprint.setOnClickListener(this);
        btn_menos.setOnClickListener(this);
        btn_mais.setOnClickListener(this);
        rg_valor.setOnCheckedChangeListener(this);
    }

    @Override
    public void init() {
        rtPrinter = BaseApplication.getInstance().getRtPrinter();
        textSetting = new TextSetting();
    }

    private void textPrint() throws UnsupportedEncodingException, JSONException {

        /* conjunto de text settings
        textSetting.setAlign(CommonEnum.ALIGN_MIDDLE);//Alinhamento - esquerda, centro, direita
        textSetting.setBold(SettingEnum.Enable);//Negrito
        textSetting.setUnderline(SettingEnum.Disable);//Sublinhado
        textSetting.setIsAntiWhite(SettingEnum.Disable);//Anti-branco
        textSetting.setDoubleHeight(SettingEnum.Enable);//Altura dupla
        textSetting.setDoubleWidth(SettingEnum.Enable);//Largura dupla
        textSetting.setItalic(SettingEnum.Disable);//itálico
        textSetting.setIsEscSmallCharactor(SettingEnum.Disable);//Fonte pequena
         */

            try {
                String[] vJogo2 = vJogo.split(Pattern.quote("$"));
                String retorno = new RequestImprimir(nJogos,Double.parseDouble(vJogo2[1].replace(",","."))).execute().get();
                if(retorno == null){
                    Toast.makeText(getApplicationContext(), "Erro", Toast.LENGTH_SHORT).show();
                    return;
                }
                CmdFactory escFac = new EscFactory();
                Cmd escCmd = escFac.create();
                escCmd.setChartsetName("UTF-8");
                TextSetting textSetting = new TextSetting();

                JSONObject json = new JSONObject(retorno);

                escCmd.append(escCmd.getHeaderCmd()); //inicia a impressão

                for (int i = 1; i <= json.length(); i++) {
                    JSONObject linhai = new JSONObject();
                    linhai=json.optJSONObject(String.valueOf(i));
                    try {
                        switch (linhai.opt("tipo").toString()){
                            case "1":
                                textSetting.setIsEscSmallCharactor(SettingEnum.Disable);
                                textSetting.setBold(SettingEnum.Disable);//Negrito
                                textSetting.setDoubleHeight(SettingEnum.Disable);//Altura dupla
                                textSetting.setDoubleWidth(SettingEnum.Disable);//Largura dupla
                                break;
                            case "2":
                                textSetting.setBold(SettingEnum.Enable);//Negrito
                                textSetting.setDoubleHeight(SettingEnum.Enable);//Altura dupla
                                textSetting.setDoubleWidth(SettingEnum.Disable);//Largura dupla
                                break;
                            case "3":
                                textSetting.setBold(SettingEnum.Enable);//Negrito
                                textSetting.setDoubleHeight(SettingEnum.Enable);//Altura dupla
                                textSetting.setDoubleWidth(SettingEnum.Enable);//Largura dupla
                                break;
                            default:
                                break;
                        }
                        switch (linhai.opt("alinhamento").toString()){
                            case "left":
                                textSetting.setAlign(CommonEnum.ALIGN_LEFT);//Alinhamento - esquerda, meio, direita
                                break;
                            case "middle":
                                textSetting.setAlign(CommonEnum.ALIGN_MIDDLE);//Alinhamento - esquerda, meio, direita
                                break;
                            case "right":
                                textSetting.setAlign(CommonEnum.ALIGN_RIGHT);//Alinhamento - esquerda, meio, direita
                                break;
                            default:
                                break;
                        }
                        escCmd.append(escCmd.getTextCmd(textSetting,linhai.opt("texto").toString() ));
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                escCmd.append(escCmd.getLFCRCmd());
                escCmd.append(escCmd.getLFCRCmd());
                escCmd.append(escCmd.getLFCRCmd());

                escCmd.append(escCmd.getBeepCmd());

                rtPrinter.writeMsgAsync(escCmd.getAppendCmds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_txtprint:
                AlertDialog alerta;
                AlertDialog.Builder builder =new AlertDialog.Builder(this);
                builder.setTitle("Confirmar");
                String mensagemAlerta="Confirme a impressão de "+String.valueOf(nJogos)+" jogos de valor "+vJogo;
                builder.setMessage(mensagemAlerta);
                builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            textPrint();
                        } catch (UnsupportedEncodingException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alerta=builder.create();
                alerta.show();
                break;
            case R.id.btn_mais:
                nJogos=nJogos+1;
                if(nJogos>maxJogos){
                    nJogos=maxJogos;
                }
                tv_nJogos.setText(String.valueOf(nJogos));
                break;
            case R.id.btn_menos:
                nJogos=nJogos-1;
                if(nJogos<1){
                    nJogos=1;
                }
                tv_nJogos.setText(String.valueOf(nJogos));
                break;
            default:
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case R.id.rb_valor1:
                vJogo=rb_valor1.getText().toString();
                break;
            case R.id.rb_valor2:
                vJogo=rb_valor2.getText().toString();
                break;
            case R.id.rb_valor3:
                vJogo=rb_valor3.getText().toString();
                break;
            case R.id.rb_valor4:
                vJogo=rb_valor4.getText().toString();
                break;
            default:
                break;
        }
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }
}
