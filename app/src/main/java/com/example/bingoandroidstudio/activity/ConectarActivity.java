package com.example.bingoandroidstudio.activity;
//antiga main activity, faz a conexão com a impressora, pode contar lixo
import android.Manifest;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.bingoandroidstudio.R;
import com.example.bingoandroidstudio.app.BaseActivity;
import com.example.bingoandroidstudio.app.BaseApplication;
import com.example.bingoandroidstudio.dialog.BluetoothDeviceChooseDialog;
import com.example.bingoandroidstudio.dialog.UsbDeviceChooseDialog;
import com.example.bingoandroidstudio.utils.BaseEnum;
import com.example.bingoandroidstudio.utils.FuncUtils;
import com.example.bingoandroidstudio.utils.LogUtils;
import com.example.bingoandroidstudio.utils.TempletDemo;
import com.example.bingoandroidstudio.utils.TimeRecordUtils;
import com.example.bingoandroidstudio.utils.ToastUtil;
import com.example.bingoandroidstudio.view.FlowRadioGroup;
import com.rt.printerlibrary.bean.BluetoothEdrConfigBean;
import com.rt.printerlibrary.bean.PrinterStatusBean;
import com.rt.printerlibrary.bean.UsbConfigBean;
import com.rt.printerlibrary.cmd.Cmd;
import com.rt.printerlibrary.cmd.EscFactory;
import com.rt.printerlibrary.connect.PrinterInterface;
import com.rt.printerlibrary.enumerate.CommonEnum;
import com.rt.printerlibrary.enumerate.ConnectStateEnum;
import com.rt.printerlibrary.exception.SdkException;
import com.rt.printerlibrary.factory.cmd.CmdFactory;
import com.rt.printerlibrary.factory.connect.BluetoothFactory;
import com.rt.printerlibrary.factory.connect.PIFactory;
import com.rt.printerlibrary.factory.connect.UsbFactory;
import com.rt.printerlibrary.factory.printer.PrinterFactory;
import com.rt.printerlibrary.factory.printer.ThermalPrinterFactory;
import com.rt.printerlibrary.observer.PrinterObserver;
import com.rt.printerlibrary.observer.PrinterObserverManager;
import com.rt.printerlibrary.printer.RTPrinter;
import com.rt.printerlibrary.utils.ConnectListener;
import com.rt.printerlibrary.utils.PrintStatusCmd;
import com.rt.printerlibrary.utils.PrinterStatusPareseUtils;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


public class ConectarActivity extends BaseActivity implements View.OnClickListener, PrinterObserver {

    private String[] NEED_PERMISSION = {
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private List<String> NO_PERMISSION = new ArrayList<String>();
    private static final int REQUEST_CAMERA = 0;
    private final int REQUEST_ENABLE_BT = 101;
    private RadioGroup rg_cmdtype;
    private FlowRadioGroup rg_connect;
    private Button btn_txt_print, btn_print_status,btn_print_status2;
    private Button btn_disConnect, btn_connect;
    private TextView tv_device_selected;
    private Button btn_connected_list;
    private ProgressBar pb_connect;

    @BaseEnum.ConnectType
    private int checkedConType = BaseEnum.CON_BLUETOOTH;
    private RTPrinter rtPrinter = null;
    private PrinterFactory printerFactory;
    private Object configObj;
    private ArrayList<PrinterInterface> printerInterfaceArrayList = new ArrayList<>();
    private PrinterInterface curPrinterInterface = null;
    private int iprintTimes=0;
    private String TAG="MainActivity";
    private List<UsbDevice> mList;
    public UsbConfigBean usbconfigObj;

    private void CheckAllPermission() {
        NO_PERMISSION.clear();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (int i = 0; i < NEED_PERMISSION.length; i++) {
                if (checkSelfPermission(NEED_PERMISSION[i]) != PackageManager.PERMISSION_GRANTED) {
                    NO_PERMISSION.add(NEED_PERMISSION[i]);
                }
            }
            if (NO_PERMISSION.size() == 0) {
                recordVideo();
            } else {
                requestPermissions(NO_PERMISSION.toArray(new String[NO_PERMISSION.size()]), REQUEST_CAMERA);
            }
        } else {
            recordVideo();
        }

    }

    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 11:
                    connectBluetoothByMac();
            }
        }
    };


    private void recordVideo() {
        Log.d("MainActivity", "Tem permissão");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CheckAllPermission();
        setContentView(R.layout.conectar);
        initView();
        init();
        addListener();
    }

    public void initView() {
        rg_cmdtype = findViewById(R.id.rg_cmdtype);
        rg_connect = findViewById(R.id.rg_connect);
        btn_txt_print = findViewById(R.id.btn_txt_print);
        btn_connect = findViewById(R.id.btn_connect);
        btn_disConnect = findViewById(R.id.btn_disConnect);
        tv_device_selected = findViewById(R.id.tv_device_selected);
        btn_connected_list = findViewById(R.id.btn_connected_list);
        pb_connect = findViewById(R.id.pb_connect);
        btn_print_status = findViewById(R.id.btn_print_status);
        btn_print_status2 = findViewById(R.id.btn_print_status2);
    }

    public void init() {
        //Inicialize como uma impressora de agulha
        btn_print_status2.setVisibility(View.GONE);
        BaseApplication.instance.setCurrentCmdType(BaseEnum.CMD_ESC);
        printerFactory = new ThermalPrinterFactory();
        rtPrinter = printerFactory.create();
        PrinterObserverManager.getInstance().add(this);//Adicionar monitoramento de status de conexão

        rtPrinter.setPrinterInterface(curPrinterInterface);
        BaseApplication.getInstance().setRtPrinter(rtPrinter);
        checkedConType = BaseEnum.CON_BLUETOOTH;
        BaseApplication.getInstance().setCurrentConnectType(checkedConType);

    }

    public void addListener() {
        btn_txt_print.setOnClickListener(this);
        btn_connect.setOnClickListener(this);
        btn_disConnect.setOnClickListener(this);
        tv_device_selected.setOnClickListener(this);
        btn_connected_list.setOnClickListener(this);
        btn_print_status.setOnClickListener(this);
        btn_print_status2.setOnClickListener(this);
        rg_connect.check(R.id.rb_connect_bluetooth);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_txt_print:
                try {
                    textPrint();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btn_disConnect:
                doDisConnect();
                break;
            case R.id.btn_connect:
                doConnect();
                break;
            case R.id.tv_device_selected:
                showConnectDialog();
                break;
            case R.id.btn_connected_list://显示多连接
                showConnectedListDialog();
                break;
            case R.id.btn_print_status:
                break;
            case R.id.btn_print_status2:
                break;
            default:
                break;
        }
    }

    public void beepTest() {
        switch (BaseApplication.getInstance().getCurrentCmdType()) {
            case BaseEnum.CMD_ESC:
                if (rtPrinter != null) {
                    CmdFactory cmdFactory = new EscFactory();
                    Cmd cmd = cmdFactory.create();
                    cmd.append(cmd.getBeepCmd());
                    rtPrinter.writeMsgAsync(cmd.getAppendCmds());
                }
                break;
            default:
                if (rtPrinter != null) {
                    CmdFactory cmdFactory = new EscFactory();
                    Cmd cmd = cmdFactory.create();
                    cmd.append(cmd.getBeepCmd());
                    rtPrinter.writeMsgAsync(cmd.getAppendCmds());
                }
                break;
        }
    }

    /**
     * 显示已连接设备窗口
     */
    private void showConnectedListDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.dialog_title_connected_devlist);
        String[] devList = new String[printerInterfaceArrayList.size()];
        for (int i = 0; i < devList.length; i++) {
            devList[i] = printerInterfaceArrayList.get(i).getConfigObject().toString();
        }
        if (devList.length > 0) {
            dialog.setItems(devList, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    PrinterInterface printerInter = printerInterfaceArrayList.get(i);
                    tv_device_selected.setText(printerInter.getConfigObject().toString());
                    rtPrinter.setPrinterInterface(printerInter);//设置连接方式 Connection port settings
                    tv_device_selected.setTag(BaseEnum.HAS_DEVICE);
                    curPrinterInterface = printerInter;
                  //  BaseApplication.getInstance().setRtPrinter(rtPrinter);//设置全局RTPrinter
                    if (printerInter.getConnectState() == ConnectStateEnum.Connected) {
                        setPrintEnable(true);
                    } else {
                        setPrintEnable(false);
                    }}
            });
        } else {
            dialog.setMessage(R.string.pls_connect_printer_first);
        }
        dialog.setNegativeButton(R.string.dialog_cancel, null);
        dialog.show();
    }

    private void doConnect() {
        if (checkedConType!=BaseEnum.CON_USB) {
            if (Integer.parseInt(tv_device_selected.getTag().toString()) == BaseEnum.NO_DEVICE) {//选择设备
                showAlertDialog(getString(R.string.main_pls_choose_device));
                return;
            }
        }
        pb_connect.setVisibility(View.VISIBLE);
        switch (checkedConType) {
            case BaseEnum.CON_BLUETOOTH:
                TimeRecordUtils.record("Início da conexão RT：", System.currentTimeMillis());
                BluetoothEdrConfigBean bluetoothEdrConfigBean = (BluetoothEdrConfigBean) configObj;
                iprintTimes = 0;
                connectBluetooth(bluetoothEdrConfigBean);
                break;
            case BaseEnum.CON_USB:
                if (Integer.parseInt(tv_device_selected.getTag().toString()) == BaseEnum.NO_DEVICE)
                    AutoConnectUsb(); //没有选择Usb时自动连接USB
                else {
                    UsbConfigBean usbConfigBean = (UsbConfigBean) configObj;
                    connectUSB(usbConfigBean);
                }
                break;
            default:
                pb_connect.setVisibility(View.GONE);
                break;
        }
    }

    //自动连接USB
    private void AutoConnectUsb() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getUsbCount() == 1) {
                    Log.d(TAG," usbPrinter connect");
                    UsbDevice mUsbDevice = mList.get(0);
                    PendingIntent mPermissionIntent = PendingIntent.getBroadcast(
                            ConectarActivity.this,
                            0,
                            new Intent(ConectarActivity.this.getApplicationInfo().packageName),
                            0);
                    usbconfigObj = new UsbConfigBean(ConectarActivity.this, mUsbDevice, mPermissionIntent);
                    connectUSB(usbconfigObj);
                }else if(getUsbCount() == 0){
                    Log.d(TAG,"not usbPrinter connect");
                    //pb_connect.setVisibility(View.GONE);
                }else
                    showUSBDeviceChooseDialog();
            }
        }, 100);
    }

    private int getUsbCount() {
        mList = new ArrayList<>();
        UsbManager mUsbManager = (UsbManager) getApplication().getSystemService(Context.USB_SERVICE);
        HashMap<String, UsbDevice> deviceList = mUsbManager.getDeviceList();
        LogUtils.d(TAG, "deviceList size = " + deviceList.size());
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();

        while (deviceIterator.hasNext()) {
            UsbDevice device = deviceIterator.next();
            LogUtils.d(TAG, "device getDeviceName" + device.getDeviceName());
            LogUtils.d(TAG, "device getVendorId" + device.getVendorId());
            LogUtils.d(TAG, "device getProductId" + device.getProductId());
            if(isPrinterDevice(device))
                mList.add(device);
        }
        return mList.size();

    }

    private static boolean isPrinterDevice(UsbDevice device) {
        if (device == null) {
            return false;
        }
        if (device.getInterfaceCount() == 0) {
            return false;
        }
        for (int i = 0; i < device.getInterfaceCount(); i++) {
            android.hardware.usb.UsbInterface usbInterface = device.getInterface(i);
            if (usbInterface.getInterfaceClass() == UsbConstants.USB_CLASS_PRINTER) {
                return true;
            }
        }
        return false;
    }

    private void connectBluetoothByMac() {
        BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice("DC:0D:30:00:1F:79");//直接连接蓝牙mac 地址 00:00:0E:01:D0:32
        BluetoothEdrConfigBean bluetoothEdrConfigBean = new BluetoothEdrConfigBean(device);
        PIFactory piFactory = new BluetoothFactory();
        PrinterInterface printerInterface = piFactory.create();
        printerInterface.setConfigObject(bluetoothEdrConfigBean);
        rtPrinter.setPrinterInterface(printerInterface);
        rtPrinter.setConnectListener(new ConnectListener() { //必须在connect之后，设置监听
                @Override
                public void onPrinterConnected(Object configObj) {
                    try {
                        TempletDemo.getInstance (rtPrinter, ConectarActivity.this).escTemplet();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (SdkException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    iprintTimes++;
                }

                @Override
                public void onPrinterDisconnect(Object configObj) {
                if (iprintTimes<5){
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Message message=new Message();
                            message.what=11;
                            handler.sendMessage(message);
                        }
                    }).start();

                }
            }

            @Override
            public void onPrinterWritecompletion(Object configObj) { //写入完成时,断开,要改为收到打印完成状态
                try {
                    Thread.sleep(500);//根据打印内容的长短，来设置 Set according to the length of the printed content
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                rtPrinter.disConnect();
            }
        });
        try {
            rtPrinter.connect(bluetoothEdrConfigBean);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void connectBluetooth(BluetoothEdrConfigBean bluetoothEdrConfigBean) {
        PIFactory piFactory = new BluetoothFactory();
        PrinterInterface printerInterface = piFactory.create();
        printerInterface.setConfigObject(bluetoothEdrConfigBean);
        rtPrinter.setPrinterInterface(printerInterface);
        try {
            rtPrinter.connect(bluetoothEdrConfigBean);
         } catch (Exception e) {
            e.printStackTrace();
           // Log.e("mao",e.getMessage());
        } finally {
        }
    }

    private void connectUSB(UsbConfigBean usbConfigBean) {
        UsbManager mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        PIFactory piFactory = new UsbFactory();
        PrinterInterface printerInterface = piFactory.create();
        printerInterface.setConfigObject(usbConfigBean);
        rtPrinter.setPrinterInterface(printerInterface);

        if (mUsbManager.hasPermission(usbConfigBean.usbDevice)) {
            try {
                rtPrinter.connect(usbConfigBean);
                BaseApplication.instance.setRtPrinter(rtPrinter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            mUsbManager.requestPermission(usbConfigBean.usbDevice, usbConfigBean.pendingIntent);
        }

    }

    private void doDisConnect() {
        if (Integer.parseInt(tv_device_selected.getTag().toString()) == BaseEnum.NO_DEVICE) {//Nenhum dispositivo selecionado
//            showAlertDialog(getString(R.string.main_discon_click_repeatedly));
            return;
        }

        if (rtPrinter != null && rtPrinter.getPrinterInterface() != null) {
            rtPrinter.disConnect();
           // DisconnectByATDISC();
        }
        tv_device_selected.setText(getString(R.string.please_connect));
        tv_device_selected.setTag(BaseEnum.NO_DEVICE);
        setPrintEnable(false);
    }

    private void showConnectDialog() {
        switch (checkedConType) {
            case BaseEnum.CON_BLUETOOTH:
                if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) { //蓝牙未开启，则开启蓝牙
                    Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                } else {
                    showBluetoothDeviceChooseDialog();
                }
                break;
            case BaseEnum.CON_USB:
                showUSBDeviceChooseDialog();
                break;
            default:
                break;
        }
    }

    private void textPrint() throws UnsupportedEncodingException {
        switch (BaseApplication.getInstance().getCurrentCmdType()) {
            case BaseEnum.CMD_ESC:
                turn2Activity(ImprimirActivity.class);
                break;
            default:
                break;
        }
    }

    @Override
    public void printerObserverCallback(final PrinterInterface printerInterface, final int state) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pb_connect.setVisibility(View.GONE);
                switch (state) {
                    case CommonEnum.CONNECT_STATE_SUCCESS:
                        TimeRecordUtils.record("RT连接end：", System.currentTimeMillis());
                        showToast(printerInterface.getConfigObject().toString() + getString(R.string._main_connected));
                        tv_device_selected.setText(printerInterface.getConfigObject().toString());
                        tv_device_selected.setTag(BaseEnum.HAS_DEVICE);
                        curPrinterInterface = printerInterface;//设置为当前连接， set current Printer Interface
                        printerInterfaceArrayList.add(printerInterface);//多连接-添加到已连接列表
                        rtPrinter.setPrinterInterface(printerInterface);
                      //  BaseApplication.getInstance().setRtPrinter(rtPrinter);
                        setPrintEnable(true);
                        break;
                    case CommonEnum.CONNECT_STATE_INTERRUPTED:
                        if (printerInterface != null && printerInterface.getConfigObject() != null) {
                            showToast(printerInterface.getConfigObject().toString() + getString(R.string._main_disconnect));
                        } else {
                            showToast(getString(R.string._main_disconnect));
                        }
                        TimeRecordUtils.record("RT连接断开：", System.currentTimeMillis());
                        tv_device_selected.setText(R.string.please_connect);
                        tv_device_selected.setTag(BaseEnum.NO_DEVICE);
                        curPrinterInterface = null;
                        printerInterfaceArrayList.remove(printerInterface);//多连接-从已连接列表中移除
                      //  BaseApplication.getInstance().setRtPrinter(null);
                        setPrintEnable(false);

                        break;
                    default:
                        break;
                }
            }
        });
    }

    @Override
    public void printerReadMsgCallback(PrinterInterface printerInterface, final byte[] bytes) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                PrinterStatusBean StatusBean = PrinterStatusPareseUtils.parsePrinterStatusResult(bytes);
                    if (StatusBean.printStatusCmd== PrintStatusCmd.cmd_PrintFinish){
                    if (StatusBean.blPrintSucc){
                      //  Log.e("mydebug","print ok");
                        ToastUtil.show(ConectarActivity.this,"print ok");
                    } else
                    {
                        ToastUtil.show(ConectarActivity.this,PrinterStatusPareseUtils.getPrinterStatusStr(StatusBean));
                    }
                } else  if (StatusBean.printStatusCmd== PrintStatusCmd.cmd_Normal){
                    ToastUtil.show(ConectarActivity.this,"print status："+PrinterStatusPareseUtils.getPrinterStatusStr(StatusBean));

                }  else  if (StatusBean.printStatusCmd== PrintStatusCmd.cmd_Print_RPP02N){
                        String msg= FuncUtils.PrinterStatusToStr(StatusBean);
                        if (!msg.isEmpty())
                            ToastUtil.show(ConectarActivity.this, "print status：" + msg);
                    }
            }
        });
    }

    private void showBluetoothDeviceChooseDialog() {
        BluetoothDeviceChooseDialog bluetoothDeviceChooseDialog = new BluetoothDeviceChooseDialog();
        bluetoothDeviceChooseDialog.setOnDeviceItemClickListener(new BluetoothDeviceChooseDialog.onDeviceItemClickListener() {
            @Override
            public void onDeviceItemClick(BluetoothDevice device) {
                if (TextUtils.isEmpty(device.getName())) {
                    tv_device_selected.setText(device.getAddress());
                } else {
                    tv_device_selected.setText(device.getName() + " [" + device.getAddress() + "]");
                }
                configObj = new BluetoothEdrConfigBean(device);
                tv_device_selected.setTag(BaseEnum.HAS_DEVICE);
                isConfigPrintEnable(configObj);
            }
        });
        bluetoothDeviceChooseDialog.show(ConectarActivity.this.getFragmentManager(), null);
    }

    /**
     * usb设备选择
     */
    private void showUSBDeviceChooseDialog() {
        final UsbDeviceChooseDialog usbDeviceChooseDialog = new UsbDeviceChooseDialog();
        usbDeviceChooseDialog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UsbDevice mUsbDevice = (UsbDevice) parent.getAdapter().getItem(position);
                PendingIntent mPermissionIntent = PendingIntent.getBroadcast(
                        ConectarActivity.this,
                        0,
                        new Intent(ConectarActivity.this.getApplicationInfo().packageName),
                        0);
                tv_device_selected.setText(getString(R.string.adapter_usbdevice) + mUsbDevice.getDeviceId()); //+ (position + 1));
                configObj = new UsbConfigBean(BaseApplication.getInstance(), mUsbDevice, mPermissionIntent);
                tv_device_selected.setTag(BaseEnum.HAS_DEVICE);
                isConfigPrintEnable(configObj);
                usbDeviceChooseDialog.dismiss();
            }
        });
        usbDeviceChooseDialog.show(getFragmentManager(), null);
    }

    /**
     * 设置是否可进行打印操作
     *
     * @param isEnable
     */
    private void setPrintEnable(boolean isEnable) {
        btn_txt_print.setEnabled(isEnable);
        btn_connect.setEnabled(!isEnable);
        btn_disConnect.setEnabled(isEnable);
        if(isEnable) {
            beepTest();
        }
        btn_print_status.setEnabled(isEnable);
        btn_print_status2.setEnabled(isEnable);

    }

    private void isConfigPrintEnable(Object configObj) {
        if (isInConnectList(configObj)) {
            setPrintEnable(true);
        } else {
            setPrintEnable(false);
        }
    }

    private boolean isInConnectList(Object configObj) {
        boolean isInList = false;
        for (int i = 0; i < printerInterfaceArrayList.size(); i++) {
            PrinterInterface printerInterface = printerInterfaceArrayList.get(i);
            if (configObj.toString().equals(printerInterface.getConfigObject().toString())) {
                if (printerInterface.getConnectState() == ConnectStateEnum.Connected) {
                    isInList = true;
                    break;
                }
            }
        }
        return isInList;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        System.exit(0);//完全退出应用，关闭进程
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_ENABLE_BT){
            if(requestCode == RESULT_OK){
                //蓝牙已经开启
                showBluetoothDeviceChooseDialog();
            }
        }
    }
}
