package com.example.bingoandroidstudio.activity;
//atividade de login, feita do 0
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bingoandroidstudio.R;
import com.example.bingoandroidstudio.app.BaseActivity;
import com.example.bingoandroidstudio.utils.User;
import com.example.bingoandroidstudio.utils.ValidarLogin;

import java.util.concurrent.ExecutionException;

public class LoginActivity extends BaseActivity {
    Button b1;
    EditText user, pass;
    public static String Token="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        b1 = findViewById(R.id.button);
        user = findViewById(R.id.username);
        pass = findViewById(R.id.password);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = user.getText().toString();
                String senha = pass.getText().toString();
                if(usuario.length()>0 && senha.length()>0){
                    try {
                        User retorno = new ValidarLogin(usuario,senha).execute().get();
                        if(retorno == null){
                            Toast.makeText(getApplicationContext(), "Usúario ou senha invalidos", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Token = retorno.getAccessToken();
                        Toast.makeText(getApplicationContext(), "Bem Vindo "+retorno.getUser(), Toast.LENGTH_SHORT).show();
                        turn2Activity(ConectarActivity.class);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }else {
                       Toast.makeText(getApplicationContext(), "Preencha os campos de usuario e senha", Toast.LENGTH_SHORT).show();
                    }
            }
        });
    }


    @Override
    public void initView() {

    }

    @Override
    public void addListener() {

    }

    @Override
    public void init() {

    }
}
